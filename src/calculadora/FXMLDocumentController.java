
package calculadora;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
    
    @FXML
    public Label respostaF, respostaC, respostaK;
    
    @FXML
    public TextField dados;
    
    @FXML
    public Button converta;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    public void deCelsius(ActionEvent evento)
    {
        double numero, numeroC, numeroK, numeroF;
        numero = Double.parseDouble(dados.getText());
       
        numeroF = (numero * 9.0/5) + 32;
        numeroC = numero;
        numeroK = numero + 273;
        
        respostaF.setText(Double.toString(numeroF));
        respostaC.setText(Double.toString(numeroC));
        respostaK.setText(Double.toString(numeroK));
    }   
    
    public void deFah(ActionEvent evento){
        double numero, numeroC, numeroK, numeroF;
        numero = Double.parseDouble(dados.getText());
       
        numeroF = numero;
        numeroC = (numero-32) / 9.0 * 5;
        numeroK = numeroC + 273;
        
        respostaF.setText(Double.toString(numeroF));
        respostaC.setText(Double.toString(numeroC));
        respostaK.setText(Double.toString(numeroK));
    }
    
}
